/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Persistencia.ConexionBD;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Yezid Quintero
 */
public class Producto {
    
    private String nombre;
    private int id;
    private double temperatura;
    private double valorBase;
    private double costo;
    
    public Producto(String nombre, int id, double temperatura, double valorBase, double costo){
        this.nombre = nombre;
        this.id = id;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
        this.costo = costo;
    }   
    
    public Producto(){    
    }
    
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }            
    
    
    @Override
    public String toString() {
        return "Producto{" + "Id=" + id + ", Nombre=" + nombre + ", Temperatura=" + temperatura + ", ValorBase=" + valorBase + ", Costo=" + String.format("%,.2f",calcularCostoDeAlmacenamiento()) + '}';
    }
    
    public Producto getProducto(int id) {
        ConexionBD conexion = new ConexionBD();
        String sql = "select * from productos WHERE id="+id+";";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            if (rs.next()) {
                this.id = rs.getInt("Id");
                this.nombre = rs.getString("Nombre");
                this.temperatura = rs.getDouble("Temperatura");                
                this.valorBase = rs.getDouble("ValorBase");                
            }
        } catch (SQLException ex) {
            System.out.println("Error" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return this;
    }
    
    public List<Producto> listarProductos() {
        List<Producto> listaProductos = new ArrayList<>();  
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setId(rs.getInt("Id"));
                p.setNombre(rs.getString("Nombre"));
                p.setTemperatura(rs.getDouble("Temperatura"));
                p.setValorBase(rs.getDouble("ValorBase"));
                p.setCosto(rs.getDouble("Costo"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }
    
    public boolean guardarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos(Id,Nombre,Temperatura,ValorBase,Costo)"
                + "VALUES("+this.id+",'" + this.nombre + "'," + this.temperatura + ",'" + this.valorBase + "'," + this.costo + ");";
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean actualizarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE productos SET Nombre='"
                + this.nombre + "',Temperatura=" + this.temperatura
                + ",ValorBase='" + this.valorBase + "',Costo="
                + this.costo + " WHERE Id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean eliminarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM productos WHERE Id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public double calcularCostoDeAlmacenamiento(){        
        if (temperatura<25){            
            return costo = getValorBase()*1.20;
        }else{
            return costo = getValorBase()*1.10;            
        }
    }
    
}
